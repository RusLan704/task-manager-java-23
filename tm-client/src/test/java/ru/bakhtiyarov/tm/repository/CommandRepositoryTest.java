package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.repository.ICommandRepository;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.marker.UnitCategory;

import java.util.List;

@Category(UnitCategory.class)
public class CommandRepositoryTest {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Test
    public void testGetCommandList(){
        List<AbstractCommand> commands = commandRepository.getCommandList();
        Assert.assertNotNull(commands);
        Assert.assertFalse(commands.isEmpty());
    }

}
