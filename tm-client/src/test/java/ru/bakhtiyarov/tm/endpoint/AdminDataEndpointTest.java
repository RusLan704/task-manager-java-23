package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.locator.EndpointLocator;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.marker.IntegrationCategory;

import javax.xml.ws.WebServiceException;

@Category(IntegrationCategory.class)
public final class AdminDataEndpointTest {

    @NotNull
    private static final EndpointLocator endpointLocator = new Bootstrap();

    @NotNull
    private static AdminDataEndpoint adminDataEndpoint;

    @BeforeClass
    public static void initData() {
        adminDataEndpoint = endpointLocator.getAdminDataEndpoint();
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeLoadBase64WithNullSession() {
        adminDataEndpoint.loadBase64(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeSaveBase64WithNullSession() {
        adminDataEndpoint.saveBase64(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeClearSaveBase64WithNullSession() {
        adminDataEndpoint.clearBase64(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeLoadBinaryWithNullSession() {
        adminDataEndpoint.loadBinary(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeSaveBinaryWithNullSession() {
        adminDataEndpoint.saveBinary(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeClearBinaryWithNullSession() {
        adminDataEndpoint.clearBinary(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeLoadJsonWithNullSession() {
        adminDataEndpoint.loadJson(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeSaveJsonWithNullSession() {
        adminDataEndpoint.saveJson(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeClearJsonWithNullSession() {
        adminDataEndpoint.clearJson(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeLoadXmlWithNullSession() {
        adminDataEndpoint.loadXml(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeSaveXmlWithNullSession() {
        adminDataEndpoint.saveXml(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeClearXmlWithNullSession() {
        adminDataEndpoint.clearXml(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeLoadYamlWithNullSession() {
        adminDataEndpoint.loadXml(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeSaveYamlWithNullSession() {
        adminDataEndpoint.saveXml(null);
    }

    @SneakyThrows
    @Test(expected = WebServiceException.class)
    public void testNegativeClearYamlWithNullSession() {
        adminDataEndpoint.clearXml(null);
    }

}
