package ru.bakhtiyarov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class SignatureUtil {

    public static String sign(final Object value, String salt, Integer cycle) {
        try {
            final ObjectMapper objectMapper = new ObjectMapper();
            final String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public static String sign(String value, String salt, Integer cycle) {
        if (value == null || salt == null) return null;
        String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.md5(salt + result + salt);
        }
        return result;
    }

    private SignatureUtil() {
    }

}
