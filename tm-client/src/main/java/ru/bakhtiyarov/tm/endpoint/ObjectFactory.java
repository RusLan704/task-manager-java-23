
package ru.bakhtiyarov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.bakhtiyarov.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateUserByLoginPassword_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "createUserByLoginPassword");
    private final static QName _CreateUserByLoginPasswordEmail_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "createUserByLoginPasswordEmail");
    private final static QName _CreateUserByLoginPasswordEmailResponse_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "createUserByLoginPasswordEmailResponse");
    private final static QName _CreateUserByLoginPasswordResponse_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "createUserByLoginPasswordResponse");
    private final static QName _CreateUserByLoginPasswordRole_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "createUserByLoginPasswordRole");
    private final static QName _CreateUserByLoginPasswordRoleResponse_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "createUserByLoginPasswordRoleResponse");
    private final static QName _FindAllUsers_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "findAllUsers");
    private final static QName _FindAllUsersResponse_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "findAllUsersResponse");
    private final static QName _FindUserById_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "findUserById");
    private final static QName _FindUserByIdResponse_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "findUserByIdResponse");
    private final static QName _FindUserByLogin_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "findUserByLogin");
    private final static QName _FindUserByLoginResponse_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "findUserByLoginResponse");
    private final static QName _UpdateUserEmail_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "updateUserEmail");
    private final static QName _UpdateUserEmailResponse_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "updateUserEmailResponse");
    private final static QName _UpdateUserFirstName_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "updateUserFirstName");
    private final static QName _UpdateUserFirstNameResponse_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "updateUserFirstNameResponse");
    private final static QName _UpdateUserLastName_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "updateUserLastName");
    private final static QName _UpdateUserLastNameResponse_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "updateUserLastNameResponse");
    private final static QName _UpdateUserLogin_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "updateUserLogin");
    private final static QName _UpdateUserLoginResponse_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "updateUserLoginResponse");
    private final static QName _UpdateUserMiddleName_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "updateUserMiddleName");
    private final static QName _UpdateUserMiddleNameResponse_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "updateUserMiddleNameResponse");
    private final static QName _UpdateUserPassword_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "updateUserPassword");
    private final static QName _UpdateUserPasswordResponse_QNAME = new QName("http://endpoint.tm.bakhtiyarov.ru/", "updateUserPasswordResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.bakhtiyarov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateUserByLoginPassword }
     * 
     */
    public CreateUserByLoginPassword createCreateUserByLoginPassword() {
        return new CreateUserByLoginPassword();
    }

    /**
     * Create an instance of {@link CreateUserByLoginPasswordEmail }
     * 
     */
    public CreateUserByLoginPasswordEmail createCreateUserByLoginPasswordEmail() {
        return new CreateUserByLoginPasswordEmail();
    }

    /**
     * Create an instance of {@link CreateUserByLoginPasswordEmailResponse }
     * 
     */
    public CreateUserByLoginPasswordEmailResponse createCreateUserByLoginPasswordEmailResponse() {
        return new CreateUserByLoginPasswordEmailResponse();
    }

    /**
     * Create an instance of {@link CreateUserByLoginPasswordResponse }
     * 
     */
    public CreateUserByLoginPasswordResponse createCreateUserByLoginPasswordResponse() {
        return new CreateUserByLoginPasswordResponse();
    }

    /**
     * Create an instance of {@link CreateUserByLoginPasswordRole }
     * 
     */
    public CreateUserByLoginPasswordRole createCreateUserByLoginPasswordRole() {
        return new CreateUserByLoginPasswordRole();
    }

    /**
     * Create an instance of {@link CreateUserByLoginPasswordRoleResponse }
     * 
     */
    public CreateUserByLoginPasswordRoleResponse createCreateUserByLoginPasswordRoleResponse() {
        return new CreateUserByLoginPasswordRoleResponse();
    }

    /**
     * Create an instance of {@link FindAllUsers }
     * 
     */
    public FindAllUsers createFindAllUsers() {
        return new FindAllUsers();
    }

    /**
     * Create an instance of {@link FindAllUsersResponse }
     * 
     */
    public FindAllUsersResponse createFindAllUsersResponse() {
        return new FindAllUsersResponse();
    }

    /**
     * Create an instance of {@link FindUserById }
     * 
     */
    public FindUserById createFindUserById() {
        return new FindUserById();
    }

    /**
     * Create an instance of {@link FindUserByIdResponse }
     * 
     */
    public FindUserByIdResponse createFindUserByIdResponse() {
        return new FindUserByIdResponse();
    }

    /**
     * Create an instance of {@link FindUserByLogin }
     * 
     */
    public FindUserByLogin createFindUserByLogin() {
        return new FindUserByLogin();
    }

    /**
     * Create an instance of {@link FindUserByLoginResponse }
     * 
     */
    public FindUserByLoginResponse createFindUserByLoginResponse() {
        return new FindUserByLoginResponse();
    }

    /**
     * Create an instance of {@link UpdateUserEmail }
     * 
     */
    public UpdateUserEmail createUpdateUserEmail() {
        return new UpdateUserEmail();
    }

    /**
     * Create an instance of {@link UpdateUserEmailResponse }
     * 
     */
    public UpdateUserEmailResponse createUpdateUserEmailResponse() {
        return new UpdateUserEmailResponse();
    }

    /**
     * Create an instance of {@link UpdateUserFirstName }
     * 
     */
    public UpdateUserFirstName createUpdateUserFirstName() {
        return new UpdateUserFirstName();
    }

    /**
     * Create an instance of {@link UpdateUserFirstNameResponse }
     * 
     */
    public UpdateUserFirstNameResponse createUpdateUserFirstNameResponse() {
        return new UpdateUserFirstNameResponse();
    }

    /**
     * Create an instance of {@link UpdateUserLastName }
     * 
     */
    public UpdateUserLastName createUpdateUserLastName() {
        return new UpdateUserLastName();
    }

    /**
     * Create an instance of {@link UpdateUserLastNameResponse }
     * 
     */
    public UpdateUserLastNameResponse createUpdateUserLastNameResponse() {
        return new UpdateUserLastNameResponse();
    }

    /**
     * Create an instance of {@link UpdateUserLogin }
     * 
     */
    public UpdateUserLogin createUpdateUserLogin() {
        return new UpdateUserLogin();
    }

    /**
     * Create an instance of {@link UpdateUserLoginResponse }
     * 
     */
    public UpdateUserLoginResponse createUpdateUserLoginResponse() {
        return new UpdateUserLoginResponse();
    }

    /**
     * Create an instance of {@link UpdateUserMiddleName }
     * 
     */
    public UpdateUserMiddleName createUpdateUserMiddleName() {
        return new UpdateUserMiddleName();
    }

    /**
     * Create an instance of {@link UpdateUserMiddleNameResponse }
     * 
     */
    public UpdateUserMiddleNameResponse createUpdateUserMiddleNameResponse() {
        return new UpdateUserMiddleNameResponse();
    }

    /**
     * Create an instance of {@link UpdateUserPassword }
     * 
     */
    public UpdateUserPassword createUpdateUserPassword() {
        return new UpdateUserPassword();
    }

    /**
     * Create an instance of {@link UpdateUserPasswordResponse }
     * 
     */
    public UpdateUserPasswordResponse createUpdateUserPasswordResponse() {
        return new UpdateUserPasswordResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserByLoginPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "createUserByLoginPassword")
    public JAXBElement<CreateUserByLoginPassword> createCreateUserByLoginPassword(CreateUserByLoginPassword value) {
        return new JAXBElement<CreateUserByLoginPassword>(_CreateUserByLoginPassword_QNAME, CreateUserByLoginPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserByLoginPasswordEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "createUserByLoginPasswordEmail")
    public JAXBElement<CreateUserByLoginPasswordEmail> createCreateUserByLoginPasswordEmail(CreateUserByLoginPasswordEmail value) {
        return new JAXBElement<CreateUserByLoginPasswordEmail>(_CreateUserByLoginPasswordEmail_QNAME, CreateUserByLoginPasswordEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserByLoginPasswordEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "createUserByLoginPasswordEmailResponse")
    public JAXBElement<CreateUserByLoginPasswordEmailResponse> createCreateUserByLoginPasswordEmailResponse(CreateUserByLoginPasswordEmailResponse value) {
        return new JAXBElement<CreateUserByLoginPasswordEmailResponse>(_CreateUserByLoginPasswordEmailResponse_QNAME, CreateUserByLoginPasswordEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserByLoginPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "createUserByLoginPasswordResponse")
    public JAXBElement<CreateUserByLoginPasswordResponse> createCreateUserByLoginPasswordResponse(CreateUserByLoginPasswordResponse value) {
        return new JAXBElement<CreateUserByLoginPasswordResponse>(_CreateUserByLoginPasswordResponse_QNAME, CreateUserByLoginPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserByLoginPasswordRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "createUserByLoginPasswordRole")
    public JAXBElement<CreateUserByLoginPasswordRole> createCreateUserByLoginPasswordRole(CreateUserByLoginPasswordRole value) {
        return new JAXBElement<CreateUserByLoginPasswordRole>(_CreateUserByLoginPasswordRole_QNAME, CreateUserByLoginPasswordRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserByLoginPasswordRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "createUserByLoginPasswordRoleResponse")
    public JAXBElement<CreateUserByLoginPasswordRoleResponse> createCreateUserByLoginPasswordRoleResponse(CreateUserByLoginPasswordRoleResponse value) {
        return new JAXBElement<CreateUserByLoginPasswordRoleResponse>(_CreateUserByLoginPasswordRoleResponse_QNAME, CreateUserByLoginPasswordRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUsers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "findAllUsers")
    public JAXBElement<FindAllUsers> createFindAllUsers(FindAllUsers value) {
        return new JAXBElement<FindAllUsers>(_FindAllUsers_QNAME, FindAllUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUsersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "findAllUsersResponse")
    public JAXBElement<FindAllUsersResponse> createFindAllUsersResponse(FindAllUsersResponse value) {
        return new JAXBElement<FindAllUsersResponse>(_FindAllUsersResponse_QNAME, FindAllUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "findUserById")
    public JAXBElement<FindUserById> createFindUserById(FindUserById value) {
        return new JAXBElement<FindUserById>(_FindUserById_QNAME, FindUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "findUserByIdResponse")
    public JAXBElement<FindUserByIdResponse> createFindUserByIdResponse(FindUserByIdResponse value) {
        return new JAXBElement<FindUserByIdResponse>(_FindUserByIdResponse_QNAME, FindUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "findUserByLogin")
    public JAXBElement<FindUserByLogin> createFindUserByLogin(FindUserByLogin value) {
        return new JAXBElement<FindUserByLogin>(_FindUserByLogin_QNAME, FindUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "findUserByLoginResponse")
    public JAXBElement<FindUserByLoginResponse> createFindUserByLoginResponse(FindUserByLoginResponse value) {
        return new JAXBElement<FindUserByLoginResponse>(_FindUserByLoginResponse_QNAME, FindUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "updateUserEmail")
    public JAXBElement<UpdateUserEmail> createUpdateUserEmail(UpdateUserEmail value) {
        return new JAXBElement<UpdateUserEmail>(_UpdateUserEmail_QNAME, UpdateUserEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "updateUserEmailResponse")
    public JAXBElement<UpdateUserEmailResponse> createUpdateUserEmailResponse(UpdateUserEmailResponse value) {
        return new JAXBElement<UpdateUserEmailResponse>(_UpdateUserEmailResponse_QNAME, UpdateUserEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserFirstName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "updateUserFirstName")
    public JAXBElement<UpdateUserFirstName> createUpdateUserFirstName(UpdateUserFirstName value) {
        return new JAXBElement<UpdateUserFirstName>(_UpdateUserFirstName_QNAME, UpdateUserFirstName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserFirstNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "updateUserFirstNameResponse")
    public JAXBElement<UpdateUserFirstNameResponse> createUpdateUserFirstNameResponse(UpdateUserFirstNameResponse value) {
        return new JAXBElement<UpdateUserFirstNameResponse>(_UpdateUserFirstNameResponse_QNAME, UpdateUserFirstNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserLastName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "updateUserLastName")
    public JAXBElement<UpdateUserLastName> createUpdateUserLastName(UpdateUserLastName value) {
        return new JAXBElement<UpdateUserLastName>(_UpdateUserLastName_QNAME, UpdateUserLastName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserLastNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "updateUserLastNameResponse")
    public JAXBElement<UpdateUserLastNameResponse> createUpdateUserLastNameResponse(UpdateUserLastNameResponse value) {
        return new JAXBElement<UpdateUserLastNameResponse>(_UpdateUserLastNameResponse_QNAME, UpdateUserLastNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "updateUserLogin")
    public JAXBElement<UpdateUserLogin> createUpdateUserLogin(UpdateUserLogin value) {
        return new JAXBElement<UpdateUserLogin>(_UpdateUserLogin_QNAME, UpdateUserLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "updateUserLoginResponse")
    public JAXBElement<UpdateUserLoginResponse> createUpdateUserLoginResponse(UpdateUserLoginResponse value) {
        return new JAXBElement<UpdateUserLoginResponse>(_UpdateUserLoginResponse_QNAME, UpdateUserLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserMiddleName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "updateUserMiddleName")
    public JAXBElement<UpdateUserMiddleName> createUpdateUserMiddleName(UpdateUserMiddleName value) {
        return new JAXBElement<UpdateUserMiddleName>(_UpdateUserMiddleName_QNAME, UpdateUserMiddleName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserMiddleNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "updateUserMiddleNameResponse")
    public JAXBElement<UpdateUserMiddleNameResponse> createUpdateUserMiddleNameResponse(UpdateUserMiddleNameResponse value) {
        return new JAXBElement<UpdateUserMiddleNameResponse>(_UpdateUserMiddleNameResponse_QNAME, UpdateUserMiddleNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "updateUserPassword")
    public JAXBElement<UpdateUserPassword> createUpdateUserPassword(UpdateUserPassword value) {
        return new JAXBElement<UpdateUserPassword>(_UpdateUserPassword_QNAME, UpdateUserPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.bakhtiyarov.ru/", name = "updateUserPasswordResponse")
    public JAXBElement<UpdateUserPasswordResponse> createUpdateUserPasswordResponse(UpdateUserPasswordResponse value) {
        return new JAXBElement<UpdateUserPasswordResponse>(_UpdateUserPasswordResponse_QNAME, UpdateUserPasswordResponse.class, null, value);
    }

}
