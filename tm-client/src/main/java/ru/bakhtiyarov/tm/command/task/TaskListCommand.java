package ru.bakhtiyarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.Session;
import ru.bakhtiyarov.tm.endpoint.Task;
import ru.bakhtiyarov.tm.endpoint.TaskEndpoint;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.TASK_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @NotNull Session session = serviceLocator.getSessionService().getSession();
        final List<Task> tasks = taskEndpoint.findAllTasksBySession(session);
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

}
