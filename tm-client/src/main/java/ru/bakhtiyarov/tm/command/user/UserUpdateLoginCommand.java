package ru.bakhtiyarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.Session;
import ru.bakhtiyarov.tm.endpoint.User;
import ru.bakhtiyarov.tm.endpoint.UserEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class UserUpdateLoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.USER_UPDATE_LOGIN;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user login.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE LOGIN]");
        @Nullable final UserEndpoint userEndpoint = endpointLocator.getUserEndpoint();
        @NotNull Session session = serviceLocator.getSessionService().getSession();
        User user = userEndpoint.findUserById(session);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final User userUpdated = userEndpoint.updateUserLogin(session, login);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}