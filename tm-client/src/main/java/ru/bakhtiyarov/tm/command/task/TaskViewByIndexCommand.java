package ru.bakhtiyarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.Session;
import ru.bakhtiyarov.tm.endpoint.Task;
import ru.bakhtiyarov.tm.endpoint.TaskEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class TaskViewByIndexCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.TASK_VIEW_BY_INDEX;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @NotNull Session session = serviceLocator.getSessionService().getSession();
        @Nullable final Task task = taskEndpoint.findTaskOneByIndex(session, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

}
