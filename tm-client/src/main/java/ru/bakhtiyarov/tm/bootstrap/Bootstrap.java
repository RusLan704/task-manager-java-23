package ru.bakhtiyarov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.locator.EndpointLocator;
import ru.bakhtiyarov.tm.api.locator.ServiceLocator;
import ru.bakhtiyarov.tm.api.repository.ICommandRepository;
import ru.bakhtiyarov.tm.api.service.ICommandService;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.*;
import ru.bakhtiyarov.tm.exception.system.UnknownArgumentException;
import ru.bakhtiyarov.tm.exception.system.UnknownCommandException;
import ru.bakhtiyarov.tm.repository.CommandRepository;
import ru.bakhtiyarov.tm.service.CommandService;
import ru.bakhtiyarov.tm.service.SessionService;
import ru.bakhtiyarov.tm.util.TerminalUtil;

import java.lang.Exception;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements EndpointLocator, ServiceLocator {

    @NotNull
    private final ServiceLocator serviceLocator = this;

    @NotNull
    private final EndpointLocator endpointLocator = this;

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ISessionService sessionService = new SessionService();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final AdminDataEndpointService adminDataEndpointService = new AdminDataEndpointService();

    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = adminDataEndpointService.getAdminDataEndpointPort();

    @NotNull
    private final AuthEndpointService authEndpointService = new AuthEndpointService();

    @NotNull
    private final AuthEndpoint authEndpoint = authEndpointService.getAuthEndpointPort();

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    {
        registry(commandService.getCommandList());
    }

    private void registry(@NotNull final List<AbstractCommand> commands) {
        for (@NotNull final AbstractCommand command : commands) {
            registryCommands(command);
        }
    }

    private void registryCommands(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setEndpointLocator(endpointLocator, serviceLocator);
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    public void run(@Nullable final String... args) {
        System.out.println("Welcome to task manager!!!");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private boolean parseArgs(@Nullable final String... args) {
        if (args == null || args.length < 1) return false;
        @NotNull final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        try {
            command.execute();
        } catch (Exception e) {
            logError(e);
        }
    }

    private static void logError(@NotNull Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    private void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return;
        @Nullable final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new UnknownArgumentException(arg);
        try {
            argument.execute();
        } catch (Exception e) {
            logError(e);
        }
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public @NotNull AdminDataEndpoint getAdminDataEndpoint() {
        return adminDataEndpoint;
    }

    @Override
    public @NotNull AdminUserEndpoint getAdminUserEndpoint() {
        return adminUserEndpoint;
    }

    @Override
    public @NotNull SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @Override
    public @NotNull ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    public @NotNull TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    public @NotNull AuthEndpoint getAuthEndpoint() {
        return authEndpoint;
    }

    @Override
    public @NotNull UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

}