package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.service.IPropertyService;
import ru.bakhtiyarov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class PropertyServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Test
    public void testGetSessionSalt() {
        Assert.assertEquals("DSADLAJSNDA", propertyService.getSessionSalt());
    }

    @Test
    public void testGetSessionCycle() {
        Integer salt = 100;
        Assert.assertEquals(salt, propertyService.getSessionCycle());
    }

    @Test
    public void testGetServerHost() {
        Assert.assertEquals("localhost", propertyService.getServerHost());
    }

    @Test
    public void testGetServerPort() {
        Assert.assertEquals((Integer) 8080, propertyService.getServerPort());
    }

}
