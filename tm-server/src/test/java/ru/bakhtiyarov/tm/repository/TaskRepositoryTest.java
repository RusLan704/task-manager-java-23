package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.repository.ITaskRepository;
import ru.bakhtiyarov.tm.const_test.ConstTest;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.marker.UnitCategory;

import java.util.List;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private Task task;

    @Before
    public void addTask() {
        task = new Task("task", "description", "1");
        taskRepository.add(task);
    }

    @After
    public void removeTask() {
        taskRepository.clearAll();
    }

    @Test
    public void testAdd() {
        Task addedTask = taskRepository.findOneById(task.getUserId(), task.getId());
        Assert.assertNotNull(addedTask);
        Assert.assertEquals(task.getName(), addedTask.getName());
        Assert.assertEquals(task.getDescription(), addedTask.getDescription());
        Assert.assertEquals(task.getUserId(), addedTask.getUserId());
    }

    @Test
    public void testClear() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.add(task);
        taskRepository.add(task);
        taskRepository.add(task);
        taskRepository.clear(task.getUserId());
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.remove(task.getUserId(), task);
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    public void testRemoveByIndex() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        Task taskTest = taskRepository.removeOneByIndex(ConstTest.FALSE_USER_ID, 0);
        Assert.assertNull(taskTest);
        taskRepository.removeOneByIndex(task.getUserId(), 0);
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    public void testRemoveByName() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        Assert.assertNull(taskRepository.removeOneByName(ConstTest.FALSE_USER_ID, task.getName()));
        Assert.assertNull(taskRepository.removeOneByName(task.getUserId(), ConstTest.FALSE_NAME));
        taskRepository.removeOneByName(task.getUserId(), task.getName());
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.removeOneById(task.getUserId(), task.getId());
        Assert.assertEquals(0, taskRepository.findAll().size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        taskRepository.add(task);
        taskRepository.add(task);
        taskRepository.add(task);
        taskRepository.add(task);
        final List<Task> tasks = taskRepository.findAll(task.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(5, taskRepository.findAll().size());
    }

    @Test
    public void findOneByIndex() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        Task taskTest = taskRepository.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(taskTest);
        Assert.assertEquals(task.getName(), taskTest.getName());
        Assert.assertEquals(task.getUserId(), taskTest.getUserId());
        Task nullTask = taskRepository.findOneByIndex(task.getUserId(), 10);
        Assert.assertNull(nullTask);
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        Task taskTest = taskRepository.findOneById(task.getUserId(), task.getId());
        Assert.assertNotNull(taskTest);
        Assert.assertEquals(task.getUserId(), taskTest.getUserId());
        Assert.assertEquals(task.getName(), taskTest.getName());
        Task nullTask = taskRepository.findOneById(task.getUserId(), ConstTest.FALSE_ID);
        Assert.assertNull(nullTask);
    }

    @Test
    public void findOneByName() {
        Assert.assertEquals(1, taskRepository.findAll().size());
        Task taskTest = taskRepository.findOneByName(task.getUserId(), task.getName());
        Assert.assertNotNull(taskTest);
        Assert.assertEquals(task.getUserId(), taskTest.getUserId());
        Assert.assertEquals(task.getName(), taskTest.getName());
        Task nullTask = taskRepository.findOneById(task.getUserId(), ConstTest.FALSE_NAME);
        Assert.assertNull(nullTask);
    }

}
