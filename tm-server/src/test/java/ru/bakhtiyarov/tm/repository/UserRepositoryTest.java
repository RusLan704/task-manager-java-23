package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.repository.IUserRepository;
import ru.bakhtiyarov.tm.const_test.ConstTest;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private User user = new User();

    @Before
    public void addUser() {
        user.setLogin("test");
        userRepository.add(user);
    }

    @After
    public void removeUser() {
        userRepository.clearAll();
    }

    @Test
    public void testFindByLogin() {
        Assert.assertEquals(1, userRepository.findAll().size());
        User userTest = userRepository.findByLogin(user.getLogin());
        Assert.assertNotNull(userTest);
        Assert.assertEquals(user.getId(), user.getId());
        User userTest2 = userRepository.findByLogin(ConstTest.FALSE_LOGIN);
        Assert.assertNull(userTest2);
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(1, userRepository.findAll().size());
        userRepository.removeById(user.getId());
        Assert.assertEquals(0, userRepository.findAll().size());
    }

    @Test
    public void testRemoveByLogin() {
        Assert.assertEquals(1, userRepository.findAll().size());
        userRepository.removeByLogin(user.getLogin());
        Assert.assertEquals(0, userRepository.findAll().size());
    }

}