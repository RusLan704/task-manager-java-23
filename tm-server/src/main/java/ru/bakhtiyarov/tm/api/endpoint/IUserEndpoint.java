package ru.bakhtiyarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebParam;
import java.util.List;

public interface IUserEndpoint {

    @NotNull
    List<User> findAllUsers(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @Nullable
    User createUserByLoginPassword(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    );

    @Nullable
    User createUserByLoginPasswordEmail(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    );

    @Nullable
    User createUserByLoginPasswordRole(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "role", partName = "role") final Role role
    );

    @Nullable
    User findUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

    @Nullable
    User findUserById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @Nullable
    User updateUserPassword(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    );

    @Nullable
    User updateUserEmail(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "email", partName = "email") final String password
    );

    @Nullable
    User updateUserFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "firstName", partName = "firstName") final String firstName
    );

    @Nullable
    User updateUserLastName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "lastName", partName = "lastName") final String lastName
    );

    @Nullable
    User updateUserMiddleName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "middleName", partName = "middleName") final String middleName
    );

    @Nullable
    User updateUserLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

}
