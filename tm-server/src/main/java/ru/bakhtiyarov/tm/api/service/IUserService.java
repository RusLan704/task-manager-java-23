package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    List<User> findAll();

    @Nullable
    User create(@Nullable String login, @Nullable String password);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User create(@Nullable String login, @Nullable String password, Role role);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User updatePassword(@Nullable String userId, @Nullable String password);

    @Nullable
    User updateUserEmail(@Nullable String userId, @Nullable String email);

    @Nullable
    User updateUserFirstName(@Nullable String userId, @Nullable String firstName);

    @Nullable
    User updateUserLastName(@Nullable String userId, @Nullable String lastName);

    @Nullable
    User updateUserMiddleName(@Nullable String userId, @Nullable String middleName);

    @Nullable
    User updateUserLogin(@Nullable String userId, @Nullable String login);

    @Nullable
    User lockUserByLogin(@Nullable String login);

    @Nullable
    User unLockUserByLogin(@Nullable String login);

}
