package ru.bakhtiyarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @Nullable
    @WebMethod
    Session openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    );

    @NotNull
    @WebMethod
    void closeSession(
            @WebParam(name = "session", partName = "session") final Session session
    );

    @NotNull
    @WebMethod
    boolean closeSessionAll(
            @WebParam(name = "session", partName = "session") final Session session
    );

}
