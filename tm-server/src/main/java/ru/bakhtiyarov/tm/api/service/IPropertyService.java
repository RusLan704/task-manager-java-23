package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    void init();

    @NotNull
    String getSessionSalt();

    @NotNull
    Integer getSessionCycle();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

}
