package ru.bakhtiyarov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.enumeration.Role;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity implements Serializable {

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @NotNull
    private String email = "";

    @NotNull
    private String firstName = "";

    @NotNull
    private String lastName = "";

    @NotNull
    private String middleName = "";

    @Nullable
    private Role role = Role.USER;

    private Boolean locked = false;

    @NotNull
    public User(@NotNull String login, @NotNull String passwordHash, @NotNull String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

}