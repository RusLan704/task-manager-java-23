package ru.bakhtiyarov.tm.endpoint.calc;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService(endpointInterface = "ru.bakhtiyarov.tm.endpoint.calc.CalcEndpoint")
public class  Calculator implements CalcEndpoint {

    public static String URL =
            "http://127.0.0.1:8080/Calculator?wsdl";

    @WebMethod
    public int sum(
            @WebParam(name = "a") int a,
            @WebParam(name = "b") int b
    ) {
        return a + b;
    }

    public static void main(String[] args) {
        final Calculator calculator = new Calculator();
      //  Endpoint.publish(URL, calculator);
    }



}
