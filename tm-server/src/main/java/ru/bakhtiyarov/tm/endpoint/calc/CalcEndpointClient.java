package ru.bakhtiyarov.tm.endpoint.calc;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public final class CalcEndpointClient {

    private static final String NS = "http://endpoint.tm.bakhtiyarov.ru/";

    private static final String PN = "CalculatorService";

    private static CalcEndpoint getInstance() throws MalformedURLException {
        final URL url = new URL(Calculator.URL);
        final QName name = new QName(NS,PN);
        final Service service = Service.create(url,name);
        return service.getPort(CalcEndpoint.class);
    }

    public static void main(String[] args) throws MalformedURLException {
        System.out.println(getInstance().sum(200,399));
    }

}
