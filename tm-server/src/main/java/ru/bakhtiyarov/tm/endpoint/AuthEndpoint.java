package ru.bakhtiyarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.IAuthEndpoint;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @NotNull
    public AuthEndpoint() {
        super(null);
    }

    @NotNull
    public AuthEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }


    @WebMethod
    public void registry(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    ) {
        serviceLocator.getAuthService().registry(login, password, email);
    }

    @Nullable
    @WebMethod
    public String getUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return session.getUserId();
    }

}